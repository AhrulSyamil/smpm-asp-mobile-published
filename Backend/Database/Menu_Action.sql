INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (3, N'View', N'role.view', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (3, N'Add', N'role.add', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (3, N'Edit', N'role.edit', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (3, N'Delete', N'role.delete', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (4, N'View', N'user.view', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (4, N'Add ', N'user.add', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (4, N'Edit ', N'user.edit', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (4, N'Delete', N'user.delete', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (5, N'View', N'wilayah.view', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (5, N'Add', N'wilayah.add', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (5, N'Edit', N'wilayah.edit', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (5, N'Delete', N'wilayah.delete', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (26, N'View', N'vendor.view', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (26, N'Add', N'vendor.add', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (26, N'Edit', N'vendor.edit', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (26, N'Delete', N'vendor.delete', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'View', N'rpspk.list.view', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Add', N'rpspk.list.add', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Request Pemasangan', N'rpspk.request-pemasangan', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Request Pemasangan - Download Card Link Template', N'rpspk.request-pemasangan.download-card-link-template', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Request Pemasangan - Submit', N'rpspk.request-pemasangan.submit', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Request Pemasangan - View MRF', N'rpspk.request-pemasangan.view-mrf', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Request Pemasangan - View Card Link', N'rpspk.request-pemasangan.view-card-link', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Verifikasi Card Link', N'rpspk.request-pemasangan', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Verifikasi Card Link - View MRF', N'rpspk.verifikasi-card-link.view-mrf', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Verifikasi Card Link - VIew Card Link', N'rpspk.verifikasi-card-link.view-card-link', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Verifikasi Card Link - Edit Card Link', N'rpspk.verifikasi-card-link.edit-card-link', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Verifikasi Card Link - Approve Card Link', N'rpspk.verifikasi-card-link.approve-card-link', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Verifikasi Card Link - Reject Card Link', N'rpspk.verifikasi-card-link.reject-card-link', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Verifikasi Card Link - Submit', N'rpspk.verifikasi-card-link.submit', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Upload TAR & Mismer', N'rpspk.upload-tar-mismer', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Upload TAR & Mismer - Submit', N'rpspk.upload-tar-mismer.submit', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Upload TAR & Mismer - Add new TID', N'rpspk.upload-tar-mismer.add-new-tid', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Input Onwer Code', N'rpspk.input-owner-code', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Input Owner Code - Edit', N'rpspk.input-owner-code.edit', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Setting Profiling', N'rpspk.setting-profiling', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Setting Profiling - Set to Profiling', N'rpspk.setting-profiling.set-toprofiling', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Setting Profiling - Download Profiling', N'rpspk.setting-profiling.download-profiling', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Generate New SPK', N'rpspk.generate-new-spk', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Generate New SPK - Process', N'rpspk.generate-new-spk.process', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Generate New SPK - Generate', N'rpspk.generate-new-spk.generate', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Generate New SPK - Download SPK', N'rpspk.generate-new-spk.download-spk', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (8, N'View', N'list-pemasangan.view', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (8, N'Edit', N'list-pemasangan.edit', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (8, N'Renewal', N'list-pemasangan.renewal', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (8, N'Closed', N'list-pemasangan.closed', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (9, N'View', N'status-pemasangan.view', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (9, N'Detail', N'status-pemasangan.detail', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (9, N'Open', N'status-pemasangan.open', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (9, N'Open - Set to Acknowledge', N'status-pemasangan.open.set-to-acknowledge', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (9, N'Acknowledge ', N'status-pemasangan.acknowledge', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (9, N'Acknowledge - Set to Working', N'status-pemasangan.acknowledge.set-to-working', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (9, N'Working', N'status-pemasangan.working', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (9, N'Working - Save PIC', N'status-pemasangan.working.save-pic', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (9, N'Working - Failed', N'status-pemasangan.working.failed', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (9, N'Solving ', N'status-pemasangan.solving', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (9, N'Solving - Submit', N'status-pemasangan.solving.submit', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (14, N'View', N'received-out.view', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (14, N'Detail', N'received-out.detail', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (14, N'Approve', N'received-out.approve', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (14, N'Reject', N'received-out.reject', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (11, N'View', N'list-penarikan.view', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (11, N'Renewal', N'list-penarikan.renewal', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (11, N'Closed', N'llist-penarikan.closed', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (12, N'View', N'status-penarikan.view', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (12, N'Detail', N'status-penarikan.detail', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (12, N'Open', N'status-penarikan.open', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (12, N'Open - Set to Acknowledge', N'status-penarikan.open.set-to-acknowledge', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (12, N'Acknowledge', N'status-penarikan.acknowledge', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (12, N'Acknowledge - Set to Working', N'status-penarikan.acknowledge.set-to-working', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (12, N'Working', N'status-penarikan.working', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (12, N'Working - Save PIC', N'status-penarikan.working.save-pic', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (12, N'Working - Failed', N'status-penarikan.working.failed', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (12, N'Solving', N'status-penarikan.solving', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (12, N'Solving - Submit', N'status-penarikan.solving.submit', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (13, N'View', N'received-in.view', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (13, N'Detail', N'received-in.detail', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (13, N'Approve', N'received-in.approve', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (13, N'Reject', N'received-in.reject', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (21, N'View', N'preventive-maintenance.view', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (21, N'Add', N'preventive-maintenance.add', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (21, N'Upload', N'preventive-maintenance.upload', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (20, N'View', N'corrective-maintenance.view', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (20, N'Add', N'corrective-maintenance.add', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (20, N'Upload', N'corrective-maintenance.upload', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (28, N'View', N'list-merchant.view', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (28, N'Edit', N'list-merchant.edit', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (28, N'Delete', N'list-merchant.delete', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (29, N'View', N'request-maintenance-merchant.view', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (29, N'Approve', N'request-maintenance-merchant.approve', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (29, N'Reject', N'request-maintenance-merchant.reject', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (22, N'View', N'inventory-edc.view', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (22, N'Add', N'inventory-edc.add', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (22, N'Upload', N'inventory-edc.upload', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (22, N'Edit', N'inventory-edc.edit', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (22, N'Delete', N'inventory-edc.delete', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (23, N'View', N'iklan.view', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (23, N'Add', N'iklan.add', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (23, N'Edit', N'iklan.edit', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (23, N'Delete', N'iklan.delete', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (23, N'Activate', N'iklan.activate', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [created_by], [updated_by], [created_at], [updated_at]) VALUES (23, N'Deactivate', N'iklan.deactivate', 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))