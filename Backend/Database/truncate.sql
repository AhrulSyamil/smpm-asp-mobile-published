truncate table Status_Pemasangan
truncate table Status_Pemasangan_Detail
truncate table Status_Pemasangan_Failed
truncate table Status_Penarikan
truncate table Status_Penarikan_Detail
truncate table Status_Penarikan_Failed

truncate table Received_In
truncate table Received_Out

truncate table Corrective_Maintenance
truncate table Corrective_Maintenance_Detail
truncate table Corrective_Maintenance_Failed

truncate table Preventive_Maintenance
truncate table Preventive_Maintenance_Detail
truncate table Preventive_Maintenance_Failed

truncate table inventory_EDC

truncate table Merchant 
truncate table Merchant_Maintenance

--truncate table role

--truncate table role_action
--truncate table menu_role

--truncate table [User]

--truncate table vendor

--truncate table Merchant
--truncate table Merchant_Maintenance

truncate table iklan

truncate table Request_Card_Link
truncate table Request_Card_Link_Detail
truncate table Card_Link
truncate table MRF
truncate table TAR
truncate table Mismer
truncate table Owner_Code
truncate table Profiling
truncate table Generate_SPK
