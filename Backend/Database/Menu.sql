USE [BNI-Merchant]
GO
SET IDENTITY_INSERT [dbo].[Menu] ON 
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (1, N'Dashboard', N'/', N'bar-chart', N'0', 1, 1)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (2, N'Menu Management', NULL, N'users', NULL, 1, 2)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (3, N'Role', N'/menu-management/role', NULL, N'2', 1, 1)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (4, N'User', N'/menu-management/user', NULL, N'2', 1, 2)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (5, N'Wilayah', N'/menu-management/wilayah', NULL, N'2', 1, 3)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (6, N'Request Input CardLink', N'/request-pemasangan-spk/list', N'file-plus', N'0', 1, 3)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (7, N'Pemasangan', NULL, N'download', NULL, 1, 4)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (8, N'List Pemasangan', N'/pemasangan/list', NULL, N'7', 1, 1)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (9, N'Status Pemasangan', N'/pemasangan/status', NULL, N'7', 1, 2)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (10, N'Penarikan', NULL, N'upload', NULL, 1, 5)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (11, N'List Penarikan', N'/penarikan/list', NULL, N'10', 1, 1)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (12, N'Status Penarikan', N'/penarikan/status', NULL, N'10', 1, 2)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (13, N'Received In', N'/received/in', N'corner-left-down', N'0', 1, 6)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (14, N'Received Out', N'/received/out', N'corner-right-up', N'0', 1, 7)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (15, N'SLA', NULL, N'thumbs-up', NULL, 0, 8)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (16, N'Request Pemasangan', N'/sla/request-pemasangan', NULL, N'15', 1, 1)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (17, N'Request Penarikan', N'/sla/request-penarikan', NULL, N'15', 1, 2)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (18, N'PM', N'/sla/pm', NULL, N'15', 1, 3)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (19, N'CM', N'/sla/cm', NULL, N'15', 1, 4)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (20, N'Corrective Maintenance', N'/corrective-maintenance/list', N'shield-off', N'0', 1, 9)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (21, N'Preventive Maintenance', N'/preventive-maintenance/list', N'shield', N'0', 1, 10)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (22, N'Inventory', N'/inventory/edc', N'archive', N'0', 1, 12)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (23, N'Manage Iklan', N'/manage-iklan', N'monitor', N'0', 1, 13)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (24, N'Report', N'/report', N'file-text', N'0', 1, 14)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (25, N'Live Chat', N'/live-chat', N'message-square', N'0', 1, 15)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (26, N'Vendor', N'/menu-management/vendor', NULL, N'2', 1, 4)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (27, N'Merchant', NULL, N'shopping-bag', NULL, 1, 11)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (28, N'List Merchant', N'/merchant/list', NULL, N'27', 1, 1)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [icon], [parent], [active], [order]) VALUES (29, N'Request Maintenance Merchant', N'/merchant/request-maintenance', NULL, N'27', 1, 2)
GO
SET IDENTITY_INSERT [dbo].[Menu] OFF
GO
