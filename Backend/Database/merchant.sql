USE [SMPM]
GO
/****** Object:  Table [dbo].[Merchant]    Script Date: 07/06/2021 21:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Merchant](
	[id_Merchant] [int] IDENTITY(1,1) NOT NULL,
	[ORG] [varchar](255) NOT NULL,
	[KATEGORI] [varchar](255) NOT NULL,
	[KODEWILAYAH] [varchar](255) NOT NULL,
	[MID] [varchar](255) NOT NULL,
	[KODECBG] [varchar](255) NULL,
	[MERCHDBANAME] [varchar](255) NULL,
	[MERCHDBACITY] [varchar](255) NULL,
	[CONTACT] [varchar](255) NULL,
	[MEMO] [varchar](255) NULL,
	[PHONE] [varchar](255) NULL,
	[MSO] [varchar](255) NULL,
	[MMO] [varchar](255) NULL,
	[MERCHANTTYPE] [varchar](255) NULL,
	[SOURCECODE] [varchar](255) NULL,
	[MERCHANTNAME] [varchar](255) NULL,
	[ADDRESS1] [varchar](255) NULL,
	[ADDRESS2] [varchar](255) NULL,
	[ADDRESS3] [varchar](255) NULL,
	[ADDRESS4] [varchar](255) NULL,
	[ZIP] [varchar](255) NULL,
	[MCC] [varchar](255) NULL,
	[POS] [varchar](255) NULL,
	[PLAN1] [varchar](255) NULL,
	[PLAN2] [varchar](255) NULL,
	[PLAN3] [varchar](255) NULL,
	[INSTLSPL1] [varchar](255) NULL,
	[INSTLSPL2] [varchar](255) NULL,
	[AGENTBANK] [varchar](255) NULL,
	[BRANCH] [varchar](255) NULL,
	[FLAGMERCHANT] [varchar](255) NULL,
	[NAMANASABAH] [varchar](255) NULL,
	[NOMORREKENING] [varchar](255) NULL,
	[NPWP] [varchar](255) NULL,
	[NAMABANK] [varchar](255) NULL,
	[MAILORDERIND] [varchar](255) NULL,
	[VISABNI] [varchar](255) NULL,
	[VISAMCJCB] [varchar](255) NULL,
	[MCBNI] [varchar](255) NULL,
	[MCDEBITBNI] [varchar](255) NULL,
	[LOCALDEBIT] [varchar](255) NULL,
	[JCBBNI] [varchar](255) NULL,
	[RESERVED] [varchar](255) NULL,
	[PRIVATELABEL] [varchar](255) NULL,
	[BNIPREPAID] [varchar](255) NULL,
	[WHITELIST] [varchar](255) NULL,
	[BNMS] [varchar](255) NULL,
	[created_by] [int] NULL,
	[updated_by] [int] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[isDeleted] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Merchant_Maintenance]    Script Date: 07/06/2021 21:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Merchant_Maintenance](
	[id_merchant_maintenance] [int] IDENTITY(1,1) NOT NULL,
	[id_Merchant] [varchar](255) NOT NULL,
	[value_before] [varchar](255) NULL,
	[value_after] [varchar](255) NULL,
	[status] [varchar](255) NULL,
	[aprove_by] [int] NULL,
	[rejected_by] [int] NULL,
	[reason] [varchar](255) NULL,
	[created_by] [int] NULL,
	[updated_by] [int] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_merchant_maintenance] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SLA_Region]    Script Date: 07/06/2021 21:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SLA_Region](
	[id_sla] [int] IDENTITY(1,1) NOT NULL,
	[action] [varchar](255) NULL,
	[scope] [varchar](255) NULL,
	[group_region] [int] NULL,
	[hour] [int] NULL,
	[target] [int] NULL,
	[penalty] [int] NULL,
	[created_by] [int] NULL,
	[updated_by] [int] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[isDeleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_sla] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Merchant] ON 

INSERT [dbo].[Merchant] ([id_Merchant], [ORG], [KATEGORI], [KODEWILAYAH], [MID], [KODECBG], [MERCHDBANAME], [MERCHDBACITY], [CONTACT], [MEMO], [PHONE], [MSO], [MMO], [MERCHANTTYPE], [SOURCECODE], [MERCHANTNAME], [ADDRESS1], [ADDRESS2], [ADDRESS3], [ADDRESS4], [ZIP], [MCC], [POS], [PLAN1], [PLAN2], [PLAN3], [INSTLSPL1], [INSTLSPL2], [AGENTBANK], [BRANCH], [FLAGMERCHANT], [NAMANASABAH], [NOMORREKENING], [NPWP], [NAMABANK], [MAILORDERIND], [VISABNI], [VISAMCJCB], [MCBNI], [MCDEBITBNI], [LOCALDEBIT], [JCBBNI], [RESERVED], [PRIVATELABEL], [BNIPREPAID], [WHITELIST], [BNMS], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (1, N'100', N'RETAIL', N'W01', N'201900049', N'', N'IDM FMMH - SIOSAR', N'MEDAN', N'BPK.FAJAR ARIE W', N'', N'82299368970', N'912', N'A01', N'99', N'', N'IDM FMMH - SIOSAR', N'PT INDOMARCO PRISMATAMA', N'JL. SIOSAR PARTIBI', N'MEREK', N'', N'22173', N'5499', N'1', N'20', N'0', N'402', N'500000', N'999999900', N'', N'', N'00', N'INDOMARCO PRISMATAMA PT.', N'0000001444188886', N'01.337.994.6-092.000', N'BNI CAB JAKARTA KOTA', N'', N'1500', N'1650', N'1500', N'150', N'1000', N'1500', N'1650', N'0', N'0', N'', N'', 2010, 2010, CAST(N'2021-06-02T14:44:34.763' AS DateTime), CAST(N'2021-06-02T14:44:34.763' AS DateTime), NULL)
INSERT [dbo].[Merchant] ([id_Merchant], [ORG], [KATEGORI], [KODEWILAYAH], [MID], [KODECBG], [MERCHDBANAME], [MERCHDBACITY], [CONTACT], [MEMO], [PHONE], [MSO], [MMO], [MERCHANTTYPE], [SOURCECODE], [MERCHANTNAME], [ADDRESS1], [ADDRESS2], [ADDRESS3], [ADDRESS4], [ZIP], [MCC], [POS], [PLAN1], [PLAN2], [PLAN3], [INSTLSPL1], [INSTLSPL2], [AGENTBANK], [BRANCH], [FLAGMERCHANT], [NAMANASABAH], [NOMORREKENING], [NPWP], [NAMABANK], [MAILORDERIND], [VISABNI], [VISAMCJCB], [MCBNI], [MCDEBITBNI], [LOCALDEBIT], [JCBBNI], [RESERVED], [PRIVATELABEL], [BNIPREPAID], [WHITELIST], [BNMS], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (2, N'100', N'RETAIL', N'W01', N'203000258', N'', N'MS GLOW SB MBL', N'BANDAR LAMPUN', N'ARUM NDARI SEPTIANI', N'', N'82278737873', N'C59', N'A03', N'99', N'', N'MS GLOW SB MBL', N'MS GLOW', N'JALAN PANGERAN ANTASARI', N'NO.53', N'KEDAMAIAN', N'35135', N'7298', N'001', N'20', N'0', N'402', N'500000', N'999999900', N'', N'', N'0', N'IBU ARUM NDARI SEPTIANI', N'0000008227873786', N'413558560323000', N'BNI CAB TANJUNG KARANG', N'', N'2000', N'02000', N'02000', N'00150', N'01000', N'02000', N'02000', N'00000', N'00000', N'', N'', 2010, 2010, CAST(N'2021-06-02T14:44:34.940' AS DateTime), CAST(N'2021-06-02T14:44:34.940' AS DateTime), NULL)
INSERT [dbo].[Merchant] ([id_Merchant], [ORG], [KATEGORI], [KODEWILAYAH], [MID], [KODECBG], [MERCHDBANAME], [MERCHDBACITY], [CONTACT], [MEMO], [PHONE], [MSO], [MMO], [MERCHANTTYPE], [SOURCECODE], [MERCHANTNAME], [ADDRESS1], [ADDRESS2], [ADDRESS3], [ADDRESS4], [ZIP], [MCC], [POS], [PLAN1], [PLAN2], [PLAN3], [INSTLSPL1], [INSTLSPL2], [AGENTBANK], [BRANCH], [FLAGMERCHANT], [NAMANASABAH], [NOMORREKENING], [NPWP], [NAMABANK], [MAILORDERIND], [VISABNI], [VISAMCJCB], [MCBNI], [MCDEBITBNI], [LOCALDEBIT], [JCBBNI], [RESERVED], [PRIVATELABEL], [BNIPREPAID], [WHITELIST], [BNMS], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (3, N'100', N'RETAIL', N'W01', N'203000264', N'', N'CAFE  WHATS UP MBL', N'BELITUNG', N'DONNY SAPUTRA LAMSYAH', N'', N'8117170182', N'C57', N'A03', N'99', N'', N'CAFE  WHATS UP MBL', N'CAFE  WHATS UP', N'JALAN ENDEK RT.04', N'TANJUNG PANDAN', N'', N'33411', N'5812', N'001', N'20', N'0', N'402', N'500000', N'999999900', N'', N'', N'0', N'BPK DONNY SAPUTRA LAMSYAH', N'0000007894444787', N'710736398305000', N'BNI CAB PANGKALPINANG', N'', N'2000', N'02000', N'02000', N'00150', N'01000', N'02000', N'02000', N'00000', N'00000', N'', N'', 2010, 2010, CAST(N'2021-06-02T14:44:34.940' AS DateTime), CAST(N'2021-06-02T14:44:34.940' AS DateTime), NULL)
INSERT [dbo].[Merchant] ([id_Merchant], [ORG], [KATEGORI], [KODEWILAYAH], [MID], [KODECBG], [MERCHDBANAME], [MERCHDBACITY], [CONTACT], [MEMO], [PHONE], [MSO], [MMO], [MERCHANTTYPE], [SOURCECODE], [MERCHANTNAME], [ADDRESS1], [ADDRESS2], [ADDRESS3], [ADDRESS4], [ZIP], [MCC], [POS], [PLAN1], [PLAN2], [PLAN3], [INSTLSPL1], [INSTLSPL2], [AGENTBANK], [BRANCH], [FLAGMERCHANT], [NAMANASABAH], [NOMORREKENING], [NPWP], [NAMABANK], [MAILORDERIND], [VISABNI], [VISAMCJCB], [MCBNI], [MCDEBITBNI], [LOCALDEBIT], [JCBBNI], [RESERVED], [PRIVATELABEL], [BNIPREPAID], [WHITELIST], [BNMS], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (4, N'100', N'RETAIL', N'W01', N'203000266', N'', N'SHINEE  CELL MBL', N'MUNTOK', N'FERRI CANDRA', N'', N'82112348830', N'C57', N'A03', N'99', N'', N'SHINEE  CELL MBL', N'SHINE CELL', N'KP SENANG HATI', N'NO.128 KP DAENG', N'', N'33311', N'4812', N'001', N'20', N'0', N'402', N'500000', N'999999900', N'', N'', N'0', N'BPK FERRI CANDRA', N'0000001401018788', N'557865573315000', N'BNI CAB PANGKALPINANG', N'', N'2000', N'02000', N'02000', N'00150', N'01000', N'02000', N'02000', N'00000', N'00000', N'', N'', 2010, 2010, CAST(N'2021-06-02T14:44:34.940' AS DateTime), CAST(N'2021-06-02T14:44:34.940' AS DateTime), NULL)
INSERT [dbo].[Merchant] ([id_Merchant], [ORG], [KATEGORI], [KODEWILAYAH], [MID], [KODECBG], [MERCHDBANAME], [MERCHDBACITY], [CONTACT], [MEMO], [PHONE], [MSO], [MMO], [MERCHANTTYPE], [SOURCECODE], [MERCHANTNAME], [ADDRESS1], [ADDRESS2], [ADDRESS3], [ADDRESS4], [ZIP], [MCC], [POS], [PLAN1], [PLAN2], [PLAN3], [INSTLSPL1], [INSTLSPL2], [AGENTBANK], [BRANCH], [FLAGMERCHANT], [NAMANASABAH], [NOMORREKENING], [NPWP], [NAMABANK], [MAILORDERIND], [VISABNI], [VISAMCJCB], [MCBNI], [MCDEBITBNI], [LOCALDEBIT], [JCBBNI], [RESERVED], [PRIVATELABEL], [BNIPREPAID], [WHITELIST], [BNMS], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (5, N'100', N'RETAIL', N'W01', N'203000267', N'', N'THE ZURI MBL', N'OGAN KOMERING', N'HASAN TASLIM (PIC JUN IMA', N'', N'82179383840', N'C47', N'A03', N'99', N'', N'THE ZURI MBL', N'PT SWARNA ANUGRAH NUSANTARA', N'JALAN DR SUTOMO', N'TERUSAN BATURAJA TIMUR', N'', N'32111', N'7011', N'002', N'20', N'0', N'402', N'500000', N'999999900', N'', N'', N'0', N'SWARNA ANUGRAH NUSANTARA PT', N'0000001175635842', N'751688441302000', N'BNI CAB BATURAJA', N'', N'1800', N'01800', N'01800', N'00150', N'01000', N'01800', N'01800', N'00000', N'00000', N'', N'', 2010, 2010, CAST(N'2021-06-02T14:44:34.940' AS DateTime), CAST(N'2021-06-02T14:44:34.940' AS DateTime), NULL)
INSERT [dbo].[Merchant] ([id_Merchant], [ORG], [KATEGORI], [KODEWILAYAH], [MID], [KODECBG], [MERCHDBANAME], [MERCHDBACITY], [CONTACT], [MEMO], [PHONE], [MSO], [MMO], [MERCHANTTYPE], [SOURCECODE], [MERCHANTNAME], [ADDRESS1], [ADDRESS2], [ADDRESS3], [ADDRESS4], [ZIP], [MCC], [POS], [PLAN1], [PLAN2], [PLAN3], [INSTLSPL1], [INSTLSPL2], [AGENTBANK], [BRANCH], [FLAGMERCHANT], [NAMANASABAH], [NOMORREKENING], [NPWP], [NAMABANK], [MAILORDERIND], [VISABNI], [VISAMCJCB], [MCBNI], [MCDEBITBNI], [LOCALDEBIT], [JCBBNI], [RESERVED], [PRIVATELABEL], [BNIPREPAID], [WHITELIST], [BNMS], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (6, N'100', N'RETAIL', N'W01', N'203000268', N'', N'BANDOL CAFE MBL', N'BATURAJA', N'MARIO RESTU PRAYOGI', N'', N'82280604889', N'C47', N'A03', N'99', N'', N'BANDOL CAFE MBL', N'BANDOL CAFE', N'DS IV BATU KUNING', N'JALAN DR MUH HATTA', N'NO.1016 BAKUNG KAMALARAJA', N'32121', N'5812', N'001', N'20', N'0', N'402', N'500000', N'999999900', N'', N'', N'0', N'SDR MARIO RESTU PRAYOGI', N'0000001175107129', N'738956978302000', N'BNI CAB BATURAJA', N'', N'2000', N'02000', N'02000', N'00150', N'01000', N'02000', N'02000', N'00000', N'00000', N'', N'', 2010, 1, CAST(N'2021-06-02T14:44:34.940' AS DateTime), CAST(N'2021-06-02T14:45:51.367' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Merchant] OFF
GO
SET IDENTITY_INSERT [dbo].[SLA_Region] ON 

INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (1, N'PEMASANGAN
', N'MERCHANT  RETAIL
', 1, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (2, N'PEMASANGAN
', N'MERCHANT  RETAIL
', 2, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (3, N'PEMASANGAN
', N'MERCHANT  RETAIL
', 3, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (4, N'PEMASANGAN
', N'MERCHANT  RETAIL
', 4, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (5, N'PEMASANGAN
', N'MERCHANT  CHAINSTORE
', 1, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (6, N'PEMASANGAN
', N'MERCHANT  CHAINSTORE
', 2, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (7, N'PEMASANGAN
', N'MERCHANT  CHAINSTORE
', 3, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (8, N'PEMASANGAN
', N'MERCHANT  CHAINSTORE
', 4, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (9, N'PEMASANGAN
', N'MERCHANT MALL & CLUSTER PRIORITAS
', 1, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (10, N'PEMASANGAN
', N'MERCHANT MALL & CLUSTER PRIORITAS
', 2, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (11, N'PEMASANGAN
', N'MERCHANT MALL & CLUSTER PRIORITAS
', 3, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (12, N'PEMASANGAN
', N'MERCHANT MALL & CLUSTER PRIORITAS
', 4, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (13, N'PEMASANGAN
', N'AGEN
', 1, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (14, N'PEMASANGAN
', N'AGEN
', 2, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (15, N'PEMASANGAN
', N'AGEN
', 3, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (16, N'PEMASANGAN
', N'AGEN
', 4, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (17, N'CORRECTIVE MAINTENANCE
', N'RESPON TIME
', 1, 1, 2, 1, 1, 0, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-06-07T15:48:00.557' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (18, N'CORRECTIVE MAINTENANCE
', N'RESPON TIME
', 2, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (19, N'CORRECTIVE MAINTENANCE
', N'RESPON TIME
', 3, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (20, N'CORRECTIVE MAINTENANCE
', N'RESPON TIME
', 4, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (21, N'CORRECTIVE MAINTENANCE
', N'MERCHANT  RETAIL
', 1, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (22, N'CORRECTIVE MAINTENANCE
', N'MERCHANT  RETAIL
', 2, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (23, N'CORRECTIVE MAINTENANCE
', N'MERCHANT  RETAIL
', 3, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (24, N'CORRECTIVE MAINTENANCE
', N'MERCHANT  RETAIL
', 4, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (25, N'CORRECTIVE MAINTENANCE
', N'MERCHANT  CHAINSTORE
', 1, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (26, N'CORRECTIVE MAINTENANCE
', N'MERCHANT  CHAINSTORE
', 2, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (27, N'CORRECTIVE MAINTENANCE
', N'MERCHANT  CHAINSTORE
', 3, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (28, N'CORRECTIVE MAINTENANCE
', N'MERCHANT  CHAINSTORE
', 4, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (29, N'CORRECTIVE MAINTENANCE
', N'MERCHANT MALL & CLUSTER PRIORITAS
', 1, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (30, N'CORRECTIVE MAINTENANCE
', N'MERCHANT MALL & CLUSTER PRIORITAS
', 2, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (31, N'CORRECTIVE MAINTENANCE
', N'MERCHANT MALL & CLUSTER PRIORITAS
', 3, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (32, N'CORRECTIVE MAINTENANCE
', N'MERCHANT MALL & CLUSTER PRIORITAS
', 4, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (33, N'CORRECTIVE MAINTENANCE
', N'AGEN
', 1, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (34, N'CORRECTIVE MAINTENANCE
', N'AGEN
', 2, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (35, N'CORRECTIVE MAINTENANCE
', N'AGEN
', 3, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (36, N'CORRECTIVE MAINTENANCE
', N'AGEN
', 4, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (37, N'PREVENTIVE MAINTENANCE
', N'Pemeliharaan secara berkala', 1, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (97, N'PREVENTIVE MAINTENANCE
', N'Pemeliharaan secara berkala', 2, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (98, N'PREVENTIVE MAINTENANCE
', N'Pemeliharaan secara berkala', 3, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (99, N'PREVENTIVE MAINTENANCE
', N'Pemeliharaan secara berkala', 4, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (100, N'PREVENTIVE MAINTENANCE
', N'Aktifitas reinisialisasi EDC
', 1, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (101, N'PREVENTIVE MAINTENANCE
', N'Aktifitas reinisialisasi EDC
', 2, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (102, N'PREVENTIVE MAINTENANCE
', N'Aktifitas reinisialisasi EDC
', 3, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (103, N'PREVENTIVE MAINTENANCE
', N'Aktifitas reinisialisasi EDC
', 4, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (104, N'PREVENTIVE MAINTENANCE
', N'Upgrade aplikasi ', 1, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (105, N'PREVENTIVE MAINTENANCE
', N'Upgrade aplikasi ', 2, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (106, N'PREVENTIVE MAINTENANCE
', N'Upgrade aplikasi ', 3, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (107, N'PREVENTIVE MAINTENANCE
', N'Upgrade aplikasi ', 4, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (108, N'PENGGANTIAN UNIT 
', N'MERCHANT  RETAIL
', 1, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (109, N'PENGGANTIAN UNIT 
', N'MERCHANT  RETAIL
', 2, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (110, N'PENGGANTIAN UNIT 
', N'MERCHANT  RETAIL
', 3, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (111, N'PENGGANTIAN UNIT 
', N'MERCHANT  RETAIL
', 4, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (112, N'PENGGANTIAN UNIT 
', N'MERCHANT  CHAINSTORE
', 1, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (113, N'PENGGANTIAN UNIT 
', N'MERCHANT  CHAINSTORE
', 2, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (114, N'PENGGANTIAN UNIT 
', N'MERCHANT  CHAINSTORE
', 3, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (115, N'PENGGANTIAN UNIT 
', N'MERCHANT  CHAINSTORE
', 4, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (116, N'PENGGANTIAN UNIT 
', N'MERCHANT MALL & CLUSTER PRIORITAS
', 1, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (117, N'PENGGANTIAN UNIT 
', N'MERCHANT MALL & CLUSTER PRIORITAS
', 2, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (118, N'PENGGANTIAN UNIT 
', N'MERCHANT MALL & CLUSTER PRIORITAS
', 3, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (119, N'PENGGANTIAN UNIT 
', N'MERCHANT MALL & CLUSTER PRIORITAS
', 4, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (120, N'PENGGANTIAN UNIT 
', N'AGEN
', 1, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (121, N'PENGGANTIAN UNIT 
', N'AGEN
', 2, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (122, N'PENGGANTIAN UNIT 
', N'AGEN
', 3, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (123, N'PENGGANTIAN UNIT 
', N'AGEN
', 4, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (124, N'PENARIKAN
', N'Penarikan perangkat EDC', 1, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (125, N'PENARIKAN
', N'Penarikan perangkat EDC', 2, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (126, N'PENARIKAN
', N'Penarikan perangkat EDC', 3, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (127, N'PENARIKAN
', N'Penarikan perangkat EDC', 4, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (128, N'PENARIKAN
', N'Pengembalian Perangkat EDC', 1, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (129, N'PENARIKAN
', N'Pengembalian Perangkat EDC', 2, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (130, N'PENARIKAN
', N'Pengembalian Perangkat EDC', 3, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
INSERT [dbo].[SLA_Region] ([id_sla], [action], [scope], [group_region], [hour], [target], [penalty], [created_by], [updated_by], [created_at], [updated_at], [isDeleted]) VALUES (131, N'PENARIKAN
', N'Pengembalian Perangkat EDC', 4, NULL, NULL, NULL, 1, 1, CAST(N'2021-03-23T00:02:39.453' AS DateTime), CAST(N'2021-03-23T00:02:39.453' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[SLA_Region] OFF
GO
