USE [SMPM]
GO
/****** Object:  StoredProcedure [dbo].[ReportCM]    Script Date: 20/06/2021 22:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
CREATE PROCEDURE [dbo].[ReportCM]
	-- Add the parameters for the stored procedure here
	@startDate varchar(50)=null,
	@endDate varchar(50)=null,
	@wilayah varchar(50)=null,
	@vendor varchar(50)=null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	declare
	@SQLSelect varchar(max),
	@SQLWhere varchar(max),
	@SQLOrder varchar(max),
	@FullSQL varchar(max)
	
	set @SQLSelect=' 

DECLARE @cmDetailOpen TABLE
(cmId int, 
 openTime datetime
)
insert into @cmDetailOpen (cmId,openTime)
select id_cm,
cast(CONVERT(varchar, created_at, 101) as datetime)  
from Corrective_Maintenance_Detail (nolock) 
where staging=1

DECLARE @cmDetailSolved TABLE
(cmId int,  
 solvedTime datetime
)
insert into @cmDetailSolved (cmId,solvedTime)
select id_cm,created_at
from Corrective_Maintenance_Detail (nolock) 
where staging=4

DECLARE @cmDetails TABLE
(cmId int,  
 opentime datetime,
 solvedTime datetime,
 age int
)
insert into @cmDetails (cmId,opentime,solvedTime,age)
select o.cmId,o.openTime,s.solvedTime,datediff(hh,o.openTime,s.solvedtime) 
from @cmDetailOpen o left join @cmDetailSolved s on o.cmId=s.cmId


SELECT cm.id_cm, cd.openTime,dbo.func_getTargetDate(cd.opentime,sr.hour) ''targetTime'',
dbo.func_getStatusSLA(cd.solvedTime,dbo.func_getTargetDate(cd.opentime,sr.hour)) ''StatusSLA'', cd.solvedTime,cd.age,ie.scope,sr.hour,
cm.status,sr.target,gw.name_group,w.name,cm.tid,cm.mid,cm.merchant,v.code,cm.vendor
FROM Corrective_Maintenance(nolock) cm 
join @cmDetails cd on cm.id_cm=cd.cmId
join inventory_edc(nolock) ie on cm.tid=ie.tid
join wilayah(nolock) w on w.id_wilayah=ie.id_wilayah
join group_wilayah (nolock) gw on gw.id_group=w.id_group
join vendor (nolock) v on cm.vendor=v.name
join SLA_Region (nolock) sr on sr.group_region=gw.id_group '
set @sqlwhere = ' where sr.action=''CORRECTIVE MAINTENANCE''
and sr.scope=ie.scope
and cd.opentime between '''+@startDate+''' and '''+@endDate+''' '
	
	set @SQLOrder=' order by 2 ASC'

	if @vendor is not null
	begin
		set @SQLWhere=@SQLWhere+' and cm.vendor='''+@vendor+''' '
	end
	if @wilayah is not null
	begin
		set @SQLWhere=@SQLWhere+' and w.name='''+@wilayah+''' '
	end


	set @FullSQL=concat(@SQLSelect,@SQLWhere,@SQLOrder)
	
	--select @FullSQL
	exec(@FullSQL)
END

GO
/****** Object:  StoredProcedure [dbo].[ReportInventory]    Script Date: 20/06/2021 22:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
CREATE PROCEDURE [dbo].[ReportInventory]
	-- Add the parameters for the stored procedure here
	@startDate varchar(50)=null,
	@endDate varchar(50)=null,
	@wilayah varchar(50)=null,
	@vendor varchar(50)=null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	declare
	@SQLSelect varchar(max),
	@SQLWhere varchar(max),
	@SQLOrder varchar(max),
	@FullSQL varchar(max)
	
	set @SQLSelect=' 

DECLARE @wilayahalias TABLE
(idWilayah int,  
 name_wilayah VARCHAR(255)
)
insert into @wilayahalias (idWilayah,name_wilayah)
select id_wilayah,name
from Wilayah (nolock) 

DECLARE @vendoralias TABLE
(idVendor int,  
 name_vendor VARCHAR(255)
)
insert into @vendoralias (idVendor,name_vendor)
select id_vendor,name
from Vendor (nolock) 

select w.name_wilayah,  v.name_vendor, m.MERCHANTNAME, 
i.TID, i.type_edc, i.serial_number, i.kondisi_mesin,
i.kelengkapan, i.jenis_kerusakan, i.penggunaan, i.tempat,
i.scope, i.status_milik, i.status, i.tanggal_masuk
from Inventory_EDC (nolock) i
join @wilayahalias w on w.idWilayah=i.id_wilayah
join @vendoralias v on v.idVendor=i.id_vendor
join Merchant (nolock) m on m.MID=i.MID'


set @sqlwhere = ' where i.tanggal_masuk between '''+@startDate+''' and '''+@endDate+''' '
	
	set @SQLOrder=' order by 2 ASC'

	if @vendor is not null
	begin
		set @SQLWhere=@SQLWhere+' and v.name_vendor='''+@vendor+''' '
	end
	if @wilayah is not null
	begin
		set @SQLWhere=@SQLWhere+' and w.name_wilayah='''+@wilayah+''' '
	end


	set @FullSQL=concat(@SQLSelect,@SQLWhere,@SQLOrder)
	
	--select @FullSQL
	exec(@FullSQL)
END
GO
/****** Object:  StoredProcedure [dbo].[ReportPemasangan]    Script Date: 20/06/2021 22:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
CREATE PROCEDURE [dbo].[ReportPemasangan]
	-- Add the parameters for the stored procedure here
	@startDate varchar(50)=null,
	@endDate varchar(50)=null,
	@wilayah varchar(50)=null,
	@vendor varchar(50)=null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	declare
	@SQLSelect varchar(max),
	@SQLWhere varchar(max),
	@SQLOrder varchar(max),
	@FullSQL varchar(max)
	
	set @SQLSelect=' 

DECLARE @pemasanganDetailOpen TABLE
(pemasanganId int, 
 openTime datetime
)
insert into @pemasanganDetailOpen (pemasanganId,openTime)
select id_pemasangan,created_at
from Status_Pemasangan_Detail (nolock) 
where staging=1

DECLARE @pemasanganDetailSolved TABLE
(pemasanganId int,  
 solvedTime datetime
)
insert into @pemasanganDetailSolved (pemasanganId,solvedTime)
select id_pemasangan,created_at
from Status_Pemasangan_Detail (nolock) 
where staging=4

DECLARE @pemasanganDetails TABLE
(pemasanganId int,  
 opentime datetime,
 solvedTime datetime,
 age int
)
insert into @pemasanganDetails (pemasanganId,opentime,solvedTime,age)
select o.pemasanganId,o.openTime,s.solvedTime,datediff(hh,o.openTime,s.solvedtime) 
from @pemasanganDetailOpen o left join @pemasanganDetailSolved s on o.pemasanganId=s.pemasanganId

SELECT pemasangan.id_pemasangan, cd.openTime,dbo.func_getTargetDate(cd.opentime,sr.hour) as targetTime,
dbo.func_getStatusSLA(cd.solvedTime,dbo.func_getTargetDate(cd.opentime,sr.hour)) as StatusSLA, cd.solvedTime,cd.age,pemasangan.ruang_lingkup,
sr.hour, pemasangan.status,sr.target,gw.name_group,w.name as wilayah,pemasangan.tid,pemasangan.mid,pemasangan.merchant,v.name,v.code 
FROM Status_Pemasangan(nolock) pemasangan 
join wilayah(nolock) w on w.id_wilayah=pemasangan.id_wilayah
join group_wilayah (nolock) gw on gw.id_group=w.id_group
join vendor (nolock) v on pemasangan.vendor=v.code
join SLA_Region (nolock) sr on sr.group_region=gw.id_group
join @pemasanganDetails cd on pemasangan.id_pemasangan=cd.pemasanganId 
'
set @sqlwhere = ' where sr.action=''PEMASANGAN''
and sr.scope=pemasangan.ruang_lingkup
and cd.opentime between '''+@startDate+''' and '''+@endDate+''' '
	
	set @SQLOrder=' order by 2 ASC'

	if @vendor is not null
	begin
		set @SQLWhere=@SQLWhere+' and v.name='''+@vendor+''' '
	end
	if @wilayah is not null
	begin
		set @SQLWhere=@SQLWhere+' and w.name='''+@wilayah+''' '
	end


	set @FullSQL=concat(@SQLSelect,@SQLWhere,@SQLOrder)
	
	--select @FullSQL
	exec(@FullSQL)
END

GO
/****** Object:  StoredProcedure [dbo].[ReportPenarikan]    Script Date: 20/06/2021 22:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
CREATE PROCEDURE [dbo].[ReportPenarikan]
	-- Add the parameters for the stored procedure here
	@startDate varchar(50)=null,
	@endDate varchar(50)=null,
	@wilayah varchar(50)=null,
	@vendor varchar(50)=null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	declare
	@SQLSelect varchar(max),
	@SQLWhere varchar(max),
	@SQLOrder varchar(max),
	@FullSQL varchar(max)
	
	set @SQLSelect=' 

DECLARE @penarikanDetailOpen TABLE
(penarikanId int, 
 openTime datetime
)
insert into @penarikanDetailOpen (penarikanId,openTime)
select id_penarikan,created_at
from Status_penarikan_Detail (nolock) 
where staging=1

DECLARE @penarikanDetailSolved TABLE
(penarikanId int,  
 solvedTime datetime
)
insert into @penarikanDetailSolved (penarikanId,solvedTime)
select id_penarikan,created_at
from Status_penarikan_Detail (nolock) 
where staging=4

DECLARE @penarikanDetails TABLE
(penarikanId int,  
 opentime datetime,
 solvedTime datetime,
 age int
)
insert into @penarikanDetails (penarikanId,opentime,solvedTime,age)
select o.penarikanId,o.openTime,s.solvedTime,datediff(hh,o.openTime,s.solvedtime) 
from @penarikanDetailOpen o left join @penarikanDetailSolved s on o.penarikanId=s.penarikanId

SELECT penarikan.id_penarikan, cd.openTime,dbo.func_getTargetDate(cd.opentime,sr.hour) as targetTime,
dbo.func_getStatusSLA(cd.solvedTime,dbo.func_getTargetDate(cd.opentime,sr.hour)) as StatusSLA, cd.solvedTime,cd.age,penarikan.ruang_lingkup,
sr.hour, penarikan.status,sr.target,gw.name_group,w.name as wilayah,penarikan.tid,penarikan.mid,penarikan.merchant,v.name as namaVendor,v.code 
FROM Status_penarikan(nolock) penarikan 
join wilayah(nolock) w on w.id_wilayah=penarikan.id_wilayah
join group_wilayah (nolock) gw on gw.id_group=w.id_group
join vendor (nolock) v on penarikan.vendor=v.code
join SLA_Region (nolock) sr on sr.group_region=gw.id_group
join @penarikanDetails cd on penarikan.id_penarikan=cd.penarikanId 
'
set @sqlwhere = ' where sr.action=''PENARIKAN''
and sr.scope=penarikan.ruang_lingkup
and cd.opentime between '''+@startDate+''' and '''+@endDate+''' '
	
	set @SQLOrder=' order by 2 ASC'

	if @vendor is not null
	begin
		set @SQLWhere=@SQLWhere+' and penarikan.vendor='''+@vendor+''' '
	end
	if @wilayah is not null
	begin
		set @SQLWhere=@SQLWhere+' and w.name='''+@wilayah+''' '
	end


	set @FullSQL=concat(@SQLSelect,@SQLWhere,@SQLOrder)
	
	--select @FullSQL
	exec(@FullSQL)
END

GO
/****** Object:  StoredProcedure [dbo].[ReportPM]    Script Date: 20/06/2021 22:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
CREATE PROCEDURE [dbo].[ReportPM]
	-- Add the parameters for the stored procedure here
	@startDate varchar(50)=null,
	@endDate varchar(50)=null,
	@wilayah varchar(50)=null,
	@vendor varchar(50)=null
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	declare
	@SQLSelect varchar(max),
	@SQLWhere varchar(max),
	@SQLOrder varchar(max),
	@FullSQL varchar(max)
	
	set @SQLSelect=' 

DECLARE @pmDetailOpen TABLE
(pmId int, 
 openTime datetime
)
insert into @pmDetailOpen (pmId,openTime)
select id_pm,created_at
from Preventive_Maintenance_Detail (nolock) 
where staging=1

DECLARE @pmDetailSolved TABLE
(pmId int,  
 solvedTime datetime
)
insert into @pmDetailSolved (pmId,solvedTime)
select id_pm,created_at
from Preventive_Maintenance_Detail (nolock) 
where staging=4

DECLARE @pmDetails TABLE
(pmId int,  
 opentime datetime,
 solvedTime datetime,
 age int
)
insert into @pmDetails (pmId,opentime,solvedTime,age)
select o.pmId,o.openTime,s.solvedTime,datediff(hh,o.openTime,s.solvedtime) 
from @pmDetailOpen o left join @pmDetailSolved s on o.pmId=s.pmId


SELECT ie.tid, ie.mid, v.code, v.name as nama_vendor, m.MERCHDBANAME, ie.merk_edc, ie.type_edc, 
ie.serial_number,ie.status as status_inventory,EOMONTH(cd.opentime) as Target_time,
dbo.func_getStatusSLA(cd.solvedTime,EOMONTH(cd.opentime))  as StatusSLA,
ticket_number, case_type, reason, info_remark,cd.openTime, 
cd.solvedTime,cd.age,ie.scope,gw.name_group,w.name as name_wilayah
FROM Inventory_EDC ie 
left join Preventive_Maintenance(nolock) pm on ie.tid = pm.tid
left join @pmDetails cd on pm.id_pm=cd.pmId
left join Vendor(nolock) v on v.id_vendor= ie.id_vendor
left join Merchant(nolock) m on m.id_merchant = ie.mid
join wilayah(nolock) w on w.id_wilayah=ie.id_wilayah
join group_wilayah (nolock) gw on gw.id_group=w.id_group
where ie.status LIKE ''TERPASANG'' 
'
set @sqlwhere = ' AND 
cd.opentime between '''+@startDate+''' and '''+@endDate+''' '
	
	set @SQLOrder=' order by 2 ASC'

	if @vendor is not null
	begin
		set @SQLWhere=@SQLWhere+' and ie.vendor='''+@vendor+''' '
	end
	if @wilayah is not null
	begin
		set @SQLWhere=@SQLWhere+' and w.name='''+@wilayah+''' '
	end


	set @FullSQL=concat(@SQLSelect,@SQLWhere,@SQLOrder)
	
	--select @FullSQL
	exec(@FullSQL)
END


GO
