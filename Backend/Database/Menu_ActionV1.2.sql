USE [SMPM]
GO
SET IDENTITY_INSERT [dbo].[Menu_Action] OFF
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (3, N'View', N'role.view', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (3, N'Add', N'role.add', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (3, N'Edit', N'role.edit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (3, N'Delete', N'role.delete', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (4, N'View', N'user.view', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (4, N'Add ', N'user.add', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (4, N'Edit ', N'user.edit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (4, N'Delete', N'user.delete', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (5, N'View', N'wilayah.view', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (5, N'Add', N'wilayah.add', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (5, N'Edit', N'wilayah.edit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (5, N'Delete', N'wilayah.delete', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'View', N'vendor.view', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Add', N'vendor.add', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Edit', N'vendor.edit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (6, N'Delete', N'vendor.delete', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'View', N'rpspk.list.view', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Detail', N'rpspk.list.detail', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Add', N'rpspk.list.add', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Request Pemasangan', N'rpspk.request-pemasangan', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Request Pemasangan - Download Card Link Template', N'rpspk.request-pemasangan.download-card-link-template', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Request Pemasangan - Submit', N'rpspk.request-pemasangan.submit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Verifikasi Card Link', N'rpspk.verifikasi-card-link', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Verifikasi Card Link - Edit Card Link', N'rpspk.verifikasi-card-link.edit-card-link', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Verifikasi Card Link - Approve Card Link', N'rpspk.verifikasi-card-link.approve-card-link', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Verifikasi Card Link - Reject Card Link', N'rpspk.verifikasi-card-link.reject-card-link', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Verifikasi Card Link - Submit', N'rpspk.verifikasi-card-link.submit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Upload TAR & Mismer', N'rpspk.upload-tar-mismer', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Upload TAR & Mismer - Download TAR Template', N'rpspk.upload-tar-mismer.download-tar-template', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Upload TAR & Mismer - Download Mismer Template', N'rpspk.upload-tar-mismer.download-mismer-template', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Upload TAR & Mismer - Submit', N'rpspk.upload-tar-mismer.submit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Upload TAR & Mismer - Add new TID', N'rpspk.upload-tar-mismer.add-new-tid', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Input Onwer Code', N'rpspk.input-owner-code', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Input Owner Code - Edit', N'rpspk.input-owner-code.edit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Setting Profiling', N'rpspk.setting-profiling', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Setting Profiling - Upload Serial Number', N'rpspk.setting-profiling.upload-serial-number', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Setting Profiling - Set to Profiling', N'rpspk.setting-profiling.set-to-profiling', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Setting Profiling - Download Profiling', N'rpspk.setting-profiling.download-profiling', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Generate New SPK', N'rpspk.generate-new-spk', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Generate New SPK - Process', N'rpspk.generate-new-spk.process', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Generate New SPK - Generate', N'rpspk.generate-new-spk.generate', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (7, N'Generate New SPK - Download SPK', N'rpspk.generate-new-spk.download-spk', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (9, N'View', N'list-pemasangan.view', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (9, N'Edit', N'list-pemasangan.edit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (9, N'Renewal', N'list-pemasangan.renewal', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (9, N'Cancel', N'list-pemasangan.cancel', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (10, N'View', N'status-pemasangan.view', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (10, N'Detail', N'status-pemasangan.detail', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (10, N'Activity', N'status-pemasangan.activity', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (10, N'Open', N'status-pemasangan.open', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (10, N'Open - Set to Acknowledge', N'status-pemasangan.open.set-to-acknowledge', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (10, N'Acknowledge ', N'status-pemasangan.acknowledge', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (10, N'Acknowledge - Set to Working', N'status-pemasangan.acknowledge.set-to-working', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (10, N'Working', N'status-pemasangan.working', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (10, N'Working - Save PIC', N'status-pemasangan.working.save-pic', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (10, N'Working - Failed', N'status-pemasangan.working.failed', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (10, N'Solving ', N'status-pemasangan.solving', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (10, N'Solving - Submit', N'status-pemasangan.solving.submit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (15, N'View', N'received-out.view', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (15, N'Approve', N'received-out.approve', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (15, N'Reject', N'received-out.reject', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (12, N'View', N'list-penarikan.view', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (12, N'Add', N'list-penarikan.add', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (12, N'Upload', N'list-penarikan.upload', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (12, N'Renewal', N'list-penarikan.renewal', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (12, N'Cancel', N'list-penarikan.cancel', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (13, N'View', N'status-penarikan.view', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (13, N'Detail', N'status-penarikan.detail', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (13, N'Activity', N'status-penarikan.activity', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (13, N'Open', N'status-penarikan.open', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (13, N'Open - Set to Acknowledge', N'status-penarikan.open.set-to-acknowledge', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (13, N'Acknowledge', N'status-penarikan.acknowledge', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (13, N'Acknowledge - Set to Working', N'status-penarikan.acknowledge.set-to-working', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (13, N'Working', N'status-penarikan.working', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (13, N'Working - Save PIC', N'status-penarikan.working.save-pic', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (13, N'Working - Failed', N'status-penarikan.working.failed', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (13, N'Solving', N'status-penarikan.solving', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (13, N'Solving - Submit', N'status-penarikan.solving.submit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (14, N'View', N'received-in.view', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (14, N'Approve', N'received-in.approve', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (14, N'Reject', N'received-in.reject', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (17, N'View', N'preventive-maintenance.view', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (17, N'Add', N'preventive-maintenance.add', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (17, N'Upload', N'preventive-maintenance.upload', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (17, N'Detail', N'preventive-maintenance.detail', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (17, N'Activity', N'preventive-maintenance.activity', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (17, N'Open', N'preventive-maintenance.open', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (17, N'Open - Set to Acknowledge', N'preventive-maintenance.open.set-to-acknowledge', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (17, N'Acknowledge', N'preventive-maintenance.acknowledge', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (17, N'Acknowledge - Set to Working', N'preventive-maintenance.acknowledge.set-to-working', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (17, N'Working', N'preventive-maintenance.working', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (17, N'Working - Save PIC', N'preventive-maintenance.working.save-pic', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (17, N'Working - Failed', N'preventive-maintenance.working.failed', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (17, N'Solving', N'preventive-maintenance.solving', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (17, N'Solving - Submit', N'preventive-maintenance.solving.submit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (16, N'View', N'corrective-maintenance.view', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (16, N'Add', N'corrective-maintenance.add', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (16, N'Upload', N'corrective-maintenance.upload', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (16, N'Detail', N'corrective-maintenance.detail', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (16, N'Activity', N'corrective-maintenance.activity', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (16, N'Open', N'corrective-maintenance.open', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (16, N'Open - Set to Acknowledge', N'corrective-maintenance.open.set-to-acknowledge', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (16, N'Acknowledge', N'corrective-maintenance.acknowledge', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (16, N'Acknowledge - Set to Working', N'corrective-maintenance.acknowledge.set-to-working', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (16, N'Working', N'corrective-maintenance.working', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (16, N'Working - Save PIC', N'corrective-maintenance.working.save-pic', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (16, N'Working - Failed', N'corrective-maintenance.working.failed', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (16, N'Working - Replace', N'corrective-maintenance.working.replace', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (16, N'Solving', N'corrective-maintenance.solving', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (16, N'Solving - Submit', N'corrective-maintenance.solving.submit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (23, N'View', N'list-merchant.view', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (23, N'Add', N'list-merchant.add', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (23, N'Upload', N'list-merchant.upload', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (23, N'Edit', N'list-merchant.edit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (23, N'Delete', N'list-merchant.delete', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (24, N'View', N'request-maintenance-merchant.view', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (24, N'Approve', N'request-maintenance-merchant.approve', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (24, N'Reject', N'request-maintenance-merchant.reject', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (18, N'View', N'inventory-edc.view', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (18, N'Add', N'inventory-edc.add', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (18, N'Upload', N'inventory-edc.upload', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (18, N'Edit', N'inventory-edc.edit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (18, N'Delete', N'inventory-edc.delete', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (18, N'Download Excel', N'inventory-edc.download.excel', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (19, N'View', N'iklan.view', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (19, N'Add', N'iklan.add', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (19, N'Edit', N'iklan.edit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (19, N'Delete', N'iklan.delete', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (19, N'Activate', N'iklan.activate', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (19, N'Deactivate', N'iklan.deactivate', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (20, N'SLA Pemasangan', N'report.sla.pemasangan', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (20, N'SLA Penarikan', N'report.sla.penarikan', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (20, N'SLA Preventive Maintenance', N'report.sla.preventive', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (20, N'SLA Corrective Maintenance', N'report.sla.corrective', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (25, N'View', N'audit.view', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (25, N'Download Excel', N'audit.download.excel', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))



INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (26, N'Detail', N'pemasangan.detail', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (26, N'Activity', N'pemasangan.activity', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (26, N'Open', N'pemasangan.open', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (26, N'Open - Set to Acknowledge', N'pemasangan.open.set-to-acknowledge', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (26, N'Acknowledge ', N'pemasangan.acknowledge', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (26, N'Acknowledge - Set to Working', N'pemasangan.acknowledge.set-to-working', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (26, N'Working', N'pemasangan.working', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (26, N'Working - Save PIC', N'pemasangan.working.save-pic', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (26, N'Working - Failed', N'pemasangan.working.failed', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (26, N'Solving ', N'pemasangan.solving', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (26, N'Solving - Submit', N'pemasangan.solving.submit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (27, N'Approve', N'received-out.approve', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (27, N'Reject', N'received-out.reject', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (28, N'Detail', N'penarikan.detail', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (28, N'Activity', N'penarikan.activity', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (28, N'Open', N'penarikan.open', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (28, N'Open - Set to Acknowledge', N'penarikan.open.set-to-acknowledge', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (28, N'Acknowledge ', N'penarikan.acknowledge', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (28, N'Acknowledge - Set to Working', N'penarikan.acknowledge.set-to-working', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (28, N'Working', N'penarikan.working', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (28, N'Working - Save PIC', N'penarikan.working.save-pic', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (28, N'Working - Failed', N'penarikan.working.failed', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (28, N'Solving ', N'penarikan.solving', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (28, N'Solving - Submit', N'penarikan.solving.submit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (29, N'Approve', N'received-in.approve', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (29, N'Reject', N'received-in.reject', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (30, N'Detail', N'preventive.detail', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (30, N'Activity', N'preventive.activity', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (30, N'Open', N'preventive.open', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (30, N'Open - Set to Acknowledge', N'preventive.open.set-to-acknowledge', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (30, N'Acknowledge ', N'preventive.acknowledge', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (30, N'Acknowledge - Set to Working', N'preventive.acknowledge.set-to-working', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (30, N'Working', N'preventive.working', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (30, N'Working - Save PIC', N'preventive.working.save-pic', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (30, N'Working - Failed', N'preventive.working.failed', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (30, N'Solving ', N'preventive.solving', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (30, N'Solving - Submit', N'preventive.solving.submit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (31, N'Detail', N'corrective.detail', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (31, N'Activity', N'corrective.activity', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (31, N'Open', N'corrective.open', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (31, N'Open - Set to Acknowledge', N'corrective.open.set-to-acknowledge', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (31, N'Acknowledge ', N'corrective.acknowledge', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (31, N'Acknowledge - Set to Working', N'corrective.acknowledge.set-to-working', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (31, N'Working', N'corrective.working', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (31, N'Working - Save PIC', N'corrective.working.save-pic', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (31, N'Working - Failed', N'corrective.working.failed', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (31, N'Working - Replace', N'corrective.working.replace', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (31, N'Solving ', N'corrective.solving', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO
INSERT [dbo].[Menu_Action] ([id_menu], [name], [action], [isActive], [created_by], [updated_by], [created_at], [updated_at]) VALUES (31, N'Solving - Submit', N'corrective.solving.submit', 1, 1, 1, CAST(N'2021-06-17T15:00:47.583' AS DateTime), CAST(N'2021-06-17T15:00:47.583' AS DateTime))
GO