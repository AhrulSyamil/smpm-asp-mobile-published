USE [SMPM]
GO
SET IDENTITY_INSERT [dbo].[Menu] ON 
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (1, N'Dashboard', N'home', N'/', N'/dashboard', N'bar-chart', 0, N'Web', 1, 1)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (2, N'Menu Management', NULL, NULL, NULL, N'users', NULL, N'Web', 1, 2)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (3, N'Role', N'menu-management.role', N'/menu-management/role', N'/role', NULL, 2, N'Web', 1, 1)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (4, N'User', N'menu-management.user', N'/menu-management/user', N'/user', NULL, 2, N'Web', 1, 2)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (5, N'Wilayah', N'menu-management.wilayah', N'/menu-management/wilayah', N'/wilayah', NULL, 2, N'Web', 1, 3)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (6, N'Vendor', N'menu-management.vendor', N'/menu-management/vendor', N'/vendor', NULL, 2, N'Web', 1, 4)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (7, N'Request Input CardLink', N'request-pemasangan-spk.list', N'/request-pemasangan-spk/list', N'/request-pemasangan-spk', N'file-plus', 0, N'Web', 1, 3)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (8, N'Pemasangan', NULL, NULL, NULL, N'download', NULL, N'Web', 1, 4)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (9, N'List Pemasangan', N'pemasangan.list', N'/pemasangan/list', N'/list-pemasangan', NULL, 8, N'Web', 1, 1)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (10, N'Status Pemasangan', N'pemasangan.status', N'/pemasangan/status', N'/status-pemasangan', NULL, 8, N'Web', 1, 2)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (11, N'Penarikan', NULL, NULL, NULL, N'upload', NULL, N'Web', 1, 5)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (12, N'List Penarikan', N'penarikan.list', N'/penarikan/list', N'/list-penarikan', NULL, 11, N'Web', 1, 1)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (13, N'Status Penarikan', N'penarikan.status', N'/penarikan/status', N'/status-penarikan', NULL, 11, N'Web', 1, 2)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (14, N'Received In', N'received.in', N'/received/in', N'/received/out', N'corner-left-down', 0, N'Web', 1, 6)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (15, N'Received Out', N'received.out', N'/received/out', N'/received/in', N'corner-right-up', 0, N'Web', 1, 7)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (16, N'Corrective Maintenance', N'corrective-maintenance.list', N'/corrective-maintenance/list', N'/corrective-maintenance', N'shield-off', 0, N'Web', 1, 8)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (17, N'Preventive Maintenance', N'preventive-maintenance.list', N'/preventive-maintenance/list', N'/preventive-maintenance', N'shield', 0, N'Web', 1, 9)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (18, N'Inventory', N'inventory.edc', N'/inventory/edc', N'/inventory/edc', N'archive', 0, N'Web', 1, 11)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (19, N'Manage Iklan', N'manage-iklan', N'/manage-iklan', N'/iklan', N'monitor', 0, N'Web', 1, 12)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (20, N'Report', N'report', N'/report', N'/OutputReport', N'file-text', 0, N'Web', 1, 13)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (21, N'Live Chat', N'livechat', N'/live-chat', N'/chat', N'message-square', 0, N'Web', 1, 14)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (22, N'Merchant', NULL, NULL, NULL, N'shopping-bag', NULL, N'Web', 1, 10)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (23, N'List Merchant', N'merchant.list', N'/merchant/list', N'/merchant-maintenance', NULL, 22, N'Web', 1, 1)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (24, N'Request Maintenance', N'merchant.request-maintenance', N'/merchant/request-maintenance', N'/merchant-maintenance/request', NULL, 22, N'Web', 1, 2)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (25, N'Audit', N'audit', N'/audit', N'/audittrail', N'monitor', 0, N'Web', 1, 15)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (26, N'Pemasangan', N'/pemasangan', N'/pemasangan', N'/pemasangan', NULL, NULL, N'Mobile', 1, 1)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (27, N'Received Out', N'/received_out', N'/received_out', N'/received/out', NULL, NULL, N'Mobile', 1, 2)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (28, N'Penarikan', N'/penarikan', N'/penarikan', N'/penarikan', NULL, NULL, N'Mobile', 1, 3)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (29, N'Received In', N'/received_in', N'/received_in', N'/received/in', NULL, NULL, N'Mobile', 1, 4)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (30, N'Preventive Maintenance', N'/pm', N'/pm', N'/maintenance/preventive', NULL, NULL, N'Mobile', 1, 5)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (31, N'Corrective Maintenance', N'/cm', N'/cm', N'/maintenance/corrective', NULL, NULL, N'Mobile', 1, 6)
GO
INSERT [dbo].[Menu] ([id_menu], [name], [route], [path], [api_path], [icon], [parent], [platform], [active], [order]) VALUES (32, N'Inventory', N'/inventory', N'/inventory', N'/inventory/edc', NULL, NULL, N'Mobile', 1, 7)
GO
SET IDENTITY_INSERT [dbo].[Menu] OFF
GO
