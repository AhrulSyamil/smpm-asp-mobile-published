USE [BNI-Merchant]
GO
/****** Object:  UserDefinedFunction [dbo].[func_getStatusSLA]    Script Date: 20/06/2021 22:44:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Adhie
-- Create date: February 4, 2019
-- Description:	Function to get transaction Date from Recipt
-- =============================================
create FUNCTION [dbo].[func_getStatusSLA]
(
	-- Add the parameters for the function here
	@solvedTime datetime,
	@targetTime datetime
)
RETURNS varchar(50)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @statusSLA varchar(50)=null

	-- Add the T-SQL statements to compute the return value here
	if @solvedTime is null
	begin
		if @targetTime<getdate()
		begin
			set @statusSLA = 'Not Achieved'
		end
		else if @targetTime>getdate()
		begin
			set @statusSLA = 'Open/Belum Solved'
		end
	end 
	else 
	begin
		if @targetTime<@solvedTime
		begin
			set @statusSLA = 'Not Achieved'
		end
		else if @targetTime>@solvedTime
		begin
			set @statusSLA = 'Achieved'
		end
	end
	-- Return the result of the function
	RETURN @statusSLA

END
GO
/****** Object:  UserDefinedFunction [dbo].[func_getTargetDate]    Script Date: 20/06/2021 22:44:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		Adhie
-- Create date: February 4, 2019
-- Description:	Function to get transaction Date from Recipt
-- =============================================
CREATE FUNCTION [dbo].[func_getTargetDate]
(
	-- Add the parameters for the function here
	@openTime datetime,
	@hour int
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @targetDate datetime=null

	-- Add the T-SQL statements to compute the return value here
	if @hour=0
	begin
		set @targetdate = cast(left(convert(varchar(50),@openTime,121),11)+'23:59:59.000' as datetime)
	end
	else
	begin
		set @targetDate = dateadd(hh,@hour,@openTime)
	end
	-- Return the result of the function
	RETURN @targetDate

END
GO
